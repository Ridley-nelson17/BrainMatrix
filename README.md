# liOS BrainMatrix

## About
This repository contains Python implementations of some fundamental Machine Learning models and algorithms from basically scratch. Fun fact, this repository only uses around 14 libraries!

The purpose of this project is not to produce optimized and computationally efficient algorithms as possible
but rather to present the inner workings of them in a comprehendable way.

### Compatability
Circle CI Build: [![CircleCI](https://circleci.com/gh/Ridley-nelson17/BrainMatrix.svg?style=svg)](https://circleci.com/gh/Ridley-nelson17/BrainMatrix)
CodeFactor Rating: [![CodeFactor](https://www.codefactor.io/repository/github/ridley-nelson17/brainmatrix/badge)](https://www.codefactor.io/repository/github/ridley-nelson17/brainmatrix)
Downloads: [Download](https://github.com/Ridley-nelson17/BrainMatrix/tarball/master)
Open Issues: ![Open Issues](https://img.shields.io/github/issues/Ridley-nelson17/BrainMatrix?color=lightblue&label=Open%20Issues)
License: ![license](https://img.shields.io/github/license/Ridley-nelson17/BrainMatrix?color=lime)
Commits Per Month: ![Commits Per Month](https://img.shields.io/github/commit-activity/m/Ridley-nelson17/BrainMatrix)
Contributors: ![Contributors](https://img.shields.io/github/contributors/Ridley-nelson17/BrainMatrix)
Repoository Size: ![Repository Size](https://img.shields.io/github/repo-size/Ridley-nelson17/BrainMatrix)
Github Release Version: ![Git Version](https://img.shields.io/github/v/release/Ridley-nelson17/BrainMatrix?color=silver)
![Git Version](https://img.shields.io/gitlab/pipeline/Ridley-nelson17/BrainMatrix?color=silver)


## Table of Contents
- [BrainMatrix](#machine-learning-from-scratch)
  * [About](#about)
  * [Table of Contents](#table-of-contents)
  * [Installation](#installation)
  * [Examples](#examples)
    + [Polynomial Regression](#polynomial-regression)
    + [Classification With CNN](#classification-with-cnn)
    + [Density-Based Clustering](#density-based-clustering)
    + [Generating Handwritten Digits](#generating-handwritten-digits)
    + [Deep Reinforcement Learning](#deep-reinforcement-learning)
    + [Image Reconstruction With RBM](#image-reconstruction-with-rbm)
    + [Evolutionary Evolved Neural Network](#evolutionary-evolved-neural-network)
    + [Genetic Algorithm](#genetic-algorithm)
    + [Association Analysis](#association-analysis)
  * [Implementations](#implementations)
    + [Supervised Learning](#supervised-learning)
    + [Unsupervised Learning](#unsupervised-learning)
    + [Reinforcement Learning](#reinforcement-learning)
    + [Deep Learning](#deep-learning)
  * [Bug Reporting](#bug-reporting)
  * [Contact](#contact)
  

## Installation
    $ git clone https://github.com/Ridley-nelson17/BrainMatrix
    $ cd BrainMatrix
    $ python setup.py install

## Examples
### Polynomial Regression
    $ python BrainMatrix/examples/polynomial_regression.py

<p align="center">
    <img src="http://eriklindernoren.se/images/p_reg.gif" width="640"\>
</p>
<p align="center">
    Figure: Training progress of a regularized polynomial regression model fitting <br>
    temperature data measured in Linköping, Sweden 2016.
</p>

### Classification With CNN
    $ python BrainMatrix/examples/convolutional_neural_network.py

    +---------+
    | ConvNet |
    +---------+
    Input Shape: (1, 8, 8)
    +----------------------+------------+--------------+
    | Layer Type           | Parameters | Output Shape |
    +----------------------+------------+--------------+
    | Conv2D               | 160        | (16, 8, 8)   |
    | Activation (ReLU)    | 0          | (16, 8, 8)   |
    | Dropout              | 0          | (16, 8, 8)   |
    | BatchNormalization   | 2048       | (16, 8, 8)   |
    | Conv2D               | 4640       | (32, 8, 8)   |
    | Activation (ReLU)    | 0          | (32, 8, 8)   |
    | Dropout              | 0          | (32, 8, 8)   |
    | BatchNormalization   | 4096       | (32, 8, 8)   |
    | Flatten              | 0          | (2048,)      |
    | Dense                | 524544     | (256,)       |
    | Activation (ReLU)    | 0          | (256,)       |
    | Dropout              | 0          | (256,)       |
    | BatchNormalization   | 512        | (256,)       |
    | Dense                | 2570       | (10,)        |
    | Activation (Softmax) | 0          | (10,)        |
    +----------------------+------------+--------------+
    Total Parameters: 538570

    Training: 100% [------------------------------------------------------------------------] Time: 0:01:55
    Accuracy: 0.987465181058

<p align="center">
    <img src="http://eriklindernoren.se/images/mlfs_cnn1.png" width="640">
</p>
<p align="center">
    Figure: Classification of the digit dataset using CNN.
</p>

### Density-Based Clustering
    $ python BrainMatrix/examples/dbscan.py

<p align="center">
    <img src="http://eriklindernoren.se/images/mlfs_dbscan.png" width="640">
</p>
<p align="center">
    Figure: Clustering of the moons dataset using DBSCAN.
</p>

### Generating Handwritten Digits
    $ python BrainMatrix/unsupervised_learning/generative_adversarial_network.py

    +-----------+
    | Generator |
    +-----------+
    Input Shape: (100,)
    +------------------------+------------+--------------+
    | Layer Type             | Parameters | Output Shape |
    +------------------------+------------+--------------+
    | Dense                  | 25856      | (256,)       |
    | Activation (LeakyReLU) | 0          | (256,)       |
    | BatchNormalization     | 512        | (256,)       |
    | Dense                  | 131584     | (512,)       |
    | Activation (LeakyReLU) | 0          | (512,)       |
    | BatchNormalization     | 1024       | (512,)       |
    | Dense                  | 525312     | (1024,)      |
    | Activation (LeakyReLU) | 0          | (1024,)      |
    | BatchNormalization     | 2048       | (1024,)      |
    | Dense                  | 803600     | (784,)       |
    | Activation (TanH)      | 0          | (784,)       |
    +------------------------+------------+--------------+
    Total Parameters: 1489936

    +---------------+
    | Discriminator |
    +---------------+
    Input Shape: (784,)
    +------------------------+------------+--------------+
    | Layer Type             | Parameters | Output Shape |
    +------------------------+------------+--------------+
    | Dense                  | 401920     | (512,)       |
    | Activation (LeakyReLU) | 0          | (512,)       |
    | Dropout                | 0          | (512,)       |
    | Dense                  | 131328     | (256,)       |
    | Activation (LeakyReLU) | 0          | (256,)       |
    | Dropout                | 0          | (256,)       |
    | Dense                  | 514        | (2,)         |
    | Activation (Softmax)   | 0          | (2,)         |
    +------------------------+------------+--------------+
    Total Parameters: 533762


<p align="center">
    <img src="http://eriklindernoren.se/images/gan_mnist5.gif" width="640">
</p>
<p align="center">
    Figure: Training progress of a Generative Adversarial Network generating <br>
    handwritten digits.
</p>

### Deep Reinforcement Learning
    $ python BrainMatrix/examples/deep_q_network.py

    +----------------+
    | Deep Q-Network |
    +----------------+
    Input Shape: (4,)
    +-------------------+------------+--------------+
    | Layer Type        | Parameters | Output Shape |
    +-------------------+------------+--------------+
    | Dense             | 320        | (64,)        |
    | Activation (ReLU) | 0          | (64,)        |
    | Dense             | 130        | (2,)         |
    +-------------------+------------+--------------+
    Total Parameters: 450

<p align="center">
    <img src="http://eriklindernoren.se/images/mlfs_dql1.gif" width="640">
</p>
<p align="center">
    Figure: Deep Q-Network solution to the CartPole-v1 environment in OpenAI gym.
</p>

### Image Reconstruction With RBM
    $ python BrainMatrix/examples/restricted_boltzmann_machine.py

<p align="center">
    <img src="http://eriklindernoren.se/images/rbm_digits1.gif" width="640">
</p>
<p align="center">
    Figure: Shows how the network gets better during training at reconstructing <br>
    the digit 2 in the MNIST dataset.
</p>

### Evolutionary Evolved Neural Network
    $ python BrainMatrix/examples/neuroevolution.py

    +---------------+
    | Model Summary |
    +---------------+
    Input Shape: (64,)
    +----------------------+------------+--------------+
    | Layer Type           | Parameters | Output Shape |
    +----------------------+------------+--------------+
    | Dense                | 1040       | (16,)        |
    | Activation (ReLU)    | 0          | (16,)        |
    | Dense                | 170        | (10,)        |
    | Activation (Softmax) | 0          | (10,)        |
    +----------------------+------------+--------------+
    Total Parameters: 1210

    Population Size: 100
    Generations: 3000
    Mutation Rate: 0.01

    [0 Best Individual - Fitness: 3.08301, Accuracy: 10.5%]
    [1 Best Individual - Fitness: 3.08746, Accuracy: 12.0%]
    ...
    [2999 Best Individual - Fitness: 94.08513, Accuracy: 98.5%]
    Test set accuracy: 96.7%

<p align="center">
    <img src="http://eriklindernoren.se/images/evo_nn4.png" width="640">
</p>
<p align="center">
    Figure: Classification of the digit dataset by a neural network which has<br>
    been evolutionary evolved.
</p>

### Genetic Algorithm
    $ python BrainMatrix/examples/genetic_algorithm.py

    +--------+
    |   GA   |
    +--------+
    Description: Implementation of a Genetic Algorithm which aims to produce
    the user specified target string. This implementation calculates each
    candidate's fitness based on the alphabetical distance between the candidate
    and the target. A candidate is selected as a parent with probabilities proportional
    to the candidate's fitness. Reproduction is implemented as a single-point
    crossover between pairs of parents. Mutation is done by randomly assigning
    new characters with uniform probability.

    Parameters
    ----------
    Target String: 'Genetic Algorithm'
    Population Size: 100
    Mutation Rate: 0.05

    [0 Closest Candidate: 'CJqlJguPlqzvpoJmb', Fitness: 0.00]
    [1 Closest Candidate: 'MCxZxdr nlfiwwGEk', Fitness: 0.01]
    [2 Closest Candidate: 'MCxZxdm nlfiwwGcx', Fitness: 0.01]
    [3 Closest Candidate: 'SmdsAklMHn kBIwKn', Fitness: 0.01]
    [4 Closest Candidate: '  lotneaJOasWfu Z', Fitness: 0.01]
    ...
    [292 Closest Candidate: 'GeneticaAlgorithm', Fitness: 1.00]
    [293 Closest Candidate: 'GeneticaAlgorithm', Fitness: 1.00]
    [294 Answer: 'Genetic Algorithm']

### Association Analysis
    $ python BrainMatrix/examples/apriori.py
    +-------------+
    |   Apriori   |
    +-------------+
    Minimum Support: 0.25
    Minimum Confidence: 0.8
    Transactions:
        [1, 2, 3, 4]
        [1, 2, 4]
        [1, 2]
        [2, 3, 4]
        [2, 3]
        [3, 4]
        [2, 4]
    Frequent Itemsets:
        [1, 2, 3, 4, [1, 2], [1, 4], [2, 3], [2, 4], [3, 4], [1, 2, 4], [2, 3, 4]]
    Rules:
        1 -> 2 (support: 0.43, confidence: 1.0)
        4 -> 2 (support: 0.57, confidence: 0.8)
        [1, 4] -> 2 (support: 0.29, confidence: 1.0)


## Implementations
### Supervised Learning
- [Adaboost](BrainMatrix/supervised_learning/adaboost.py)
- [Bayesian Regression](BrainMatrix/supervised_learning/bayesian_regression.py)
- [Decision Tree](BrainMatrix/supervised_learning/decision_tree.py)
- [Elastic Net](BrainMatrix/supervised_learning/regression.py)
- [Gradient Boosting](BrainMatrix/supervised_learning/gradient_boosting.py)
- [K Nearest Neighbors](BrainMatrix/supervised_learning/k_nearest_neighbors.py)
- [Lasso Regression](BrainMatrix/supervised_learning/regression.py)
- [Linear Discriminant Analysis](BrainMatrix/supervised_learning/linear_discriminant_analysis.py)
- [Linear Regression](BrainMatrix/supervised_learning/regression.py)
- [Logistic Regression](BrainMatrix/supervised_learning/logistic_regression.py)
- [Multi-class Linear Discriminant Analysis](mlfromscratch/supervised_learning/multi_class_lda.py)
- [Multilayer Perceptron](BrainMatrix/supervised_learning/multilayer_perceptron.py)
- [Naive Bayes](BrainMatrix/supervised_learning/naive_bayes.py)
- [Neuroevolution](BrainMatrix/supervised_learning/neuroevolution.py)
- [Particle Swarm Optimization of Neural Network](BrainMatrix/supervised_learning/particle_swarm_optimization.py)
- [Perceptron](BrainMatrix/supervised_learning/perceptron.py)
- [Polynomial Regression](BrainMatrix/supervised_learning/regression.py)
- [Random Forest](BrainMatrix/supervised_learning/random_forest.py)
- [Ridge Regression](BrainMatrix/supervised_learning/regression.py)
- [Support Vector Machine](BrainMatrix/supervised_learning/support_vector_machine.py)
- [XGBoost](BrainMatrix/supervised_learning/xgboost.py)

### Unsupervised Learning
- [Apriori](BrainMatrix/unsupervised_learning/apriori.py)
- [Autoencoder](BrainMatrix/unsupervised_learning/autoencoder.py)
- [DBSCAN](BrainMatrix/unsupervised_learning/dbscan.py)
- [FP-Growth](BrainMatrix/unsupervised_learning/fp_growth.py)
- [Gaussian Mixture Model](BrainMatrix/unsupervised_learning/gaussian_mixture_model.py)
- [Generative Adversarial Network](BrainMatrix/unsupervised_learning/generative_adversarial_network.py)
- [Genetic Algorithm](BrainMatrix/unsupervised_learning/genetic_algorithm.py)
- [K-Means](BrainMatrix/unsupervised_learning/k_means.py)
- [Partitioning Around Medoids](BrainMatrix/unsupervised_learning/partitioning_around_medoids.py)
- [Principal Component Analysis](BrainMatrix/unsupervised_learning/principal_component_analysis.py)
- [Restricted Boltzmann Machine](BrainMatrix/unsupervised_learning/restricted_boltzmann_machine.py)

### Reinforcement Learning
- [Deep Q-Network](BrainMatrix/reinforcement_learning/deep_q_network.py)

### Deep Learning
  + [Neural Network](BrainMatrix/deep_learning/neural_network.py)
  + [Layers](BrainMatrix/deep_learning/layers.py)
    * Activation Layer
    * Average Pooling Layer
    * Batch Normalization Layer
    * Constant Padding Layer
    * Convolutional Layer
    * Dropout Layer
    * Flatten Layer
    * Fully-Connected (Dense) Layer
    * Fully-Connected RNN Layer
    * Max Pooling Layer
    * Reshape Layer
    * Up Sampling Layer
    * Zero Padding Layer
  + Model Types
    * [Convolutional Neural Network](mlfromscratch/examples/convolutional_neural_network.py)
    * [Multilayer Perceptron](mlfromscratch/examples/multilayer_perceptron.py)
    * [Recurrent Neural Network](mlfromscratch/examples/recurrent_neural_network.py)

## Bug Reporting
If you notice that anything isn't right, please note it down on the [wiki](https://github.com/Ridley-nelson17/BrainMatrix/wiki) page that I have set up. 
If you are wondering what other tools I am using, I found [Code Factor](https://www.codefactor.io) to be a very usefull tool. You do get a student/buisness discount if you are eligable on [Github Education](https://education.github.com).

## Contact
If there's some implementation you would like to see here or if you're just feeling social,
feel free to [email](mailto:ridley.nelson17@gmail.com) me.
